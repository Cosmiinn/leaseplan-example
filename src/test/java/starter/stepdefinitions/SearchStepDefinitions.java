package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Steps;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String endpoint) {
        carsAPI.callEndpoint(endpoint);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForSearchedItem(String item) {
        carsAPI.verifyTheItemIsDisplayed(item);
    }


    @Then("not found is returned")
    public void he_Doesn_Not_See_The_Results() {
        carsAPI.verifyTheResultIsNotDisplayed();
    }
}
