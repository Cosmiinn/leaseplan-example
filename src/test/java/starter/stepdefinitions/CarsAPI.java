package starter.stepdefinitions;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.Matchers.containsStringIgnoringCase;

public class CarsAPI {


    public void callEndpoint(String endpoint){
        SerenityRest.given()
                .when()
                .get(endpoint)
                .then()
                .statusCode(200);
    }


    public void verifyTheItemIsDisplayed(String item){
        Response response = SerenityRest.lastResponse();
        List<String> titles = response.jsonPath().getList("title", String.class);
        for (String title : titles) {
            System.out.println("Title: " + title);
        }

        SerenityRest.then()
                .body("title", everyItem(containsStringIgnoringCase(item)));
    }



    public void verifyTheResultIsNotDisplayed(){
        SerenityRest.then()
                .body("detail.error", equalTo(true))
                .body("detail.message", equalTo("Not found"));
    }


}
