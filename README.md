#Steps done

 1. Clean POM.xml (Using only one builder - Gradle)
 2. Update and add dependencies in the build.gradle file
 3. Change SearchStepDefinitions.java class by making the steps reusable
 4. Add the implementation in the CarsAPI.java class

 #Note: One TestCase will fail and is ok because when using cola one of the result contains as a title 

     - title: "AA Drink High energy",

#which is not right


#Steps to use

 1. The tests will automatically run on the CI/CD at each push
 2. New Scenarios can be added to the current feature file or in another feature file created in resources file
 3. The step definition class must contain the definition for the Given, When, Then steps 
 4. Also, the implementation for the step definition must be created in a separate class for a better readability and
as a best practice
 5. the command "gradle test" can be used to run locally

